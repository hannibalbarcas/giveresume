# Giveresume with [Bootstrap](http://getbootstrap.com)

Everything is in the node_modules directory. Yes I used Bootstrap. You can checkout the demo at https://giveresume.herokuapp.com/

## Install distribution
Run ```npm install``` in the bootstrap folder.
Run ```grunt dist``` (Just compile CSS and JavaScript) in the bootstrap folder.
Regenerates the /dist/ directory with compiled and minified CSS and JavaScript files. As a Bootstrap user, this is normally the command you want.


## Copyright and license

Code and documentation copyright 2011-2015 Twitter, Inc. Code released under [the MIT license](https://github.com/twbs/bootstrap/blob/master/LICENSE). Docs released under [Creative Commons](https://github.com/twbs/bootstrap/blob/master/docs/LICENSE).